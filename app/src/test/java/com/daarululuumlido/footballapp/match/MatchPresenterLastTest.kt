package com.daarululuumlido.footballapp.match

import com.daarululuumlido.footballapp.TestContextProvider
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.main.matchlast.MatchLastPresenter
import com.daarululuumlido.footballapp.main.matchlast.MatchLastView
import com.daarululuumlido.footballapp.model.Match
import com.daarululuumlido.footballapp.model.MatchResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MatchPresenterLastTest {

    @Mock
    private
    lateinit var view: MatchLastView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository
    lateinit var presenter: MatchLastPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MatchLastPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getMacthLast() {
        val event: MutableList<Match> = mutableListOf()
        val response = MatchResponse(event)
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportDBAPi.getSchedulesLast15()),
                MatchResponse::class.java
        )).thenReturn(response)
        presenter.getMacthLast()
        Mockito.verify(view).showLoading()
        Mockito.verify(view).showMatchList(event)
        Mockito.verify(view).hideLoading()
    }
}