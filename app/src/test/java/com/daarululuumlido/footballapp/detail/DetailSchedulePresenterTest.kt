package com.daarululuumlido.footballapp.detail

import com.daarululuumlido.footballapp.TestContextProvider
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.main.detailMatch.DetailSchedulePresenter
import com.daarululuumlido.footballapp.main.detailMatch.DetailScheduleView
import com.daarululuumlido.footballapp.model.Match
import com.daarululuumlido.footballapp.model.MatchResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DetailSchedulePresenterTest {

    @Mock
    private
    lateinit var view: DetailScheduleView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository
    lateinit var presenter: DetailSchedulePresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = DetailSchedulePresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getDetailSchedule() {
        val event  = mutableListOf<Match>()
        val response = MatchResponse(event)
        val id = "583799"
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportDBAPi.getSchedulesLast15()),
                MatchResponse::class.java
        )).thenReturn(response)
        presenter.getDetailSchedule(id)
        Mockito.verify(view).initData(event)
    }
}