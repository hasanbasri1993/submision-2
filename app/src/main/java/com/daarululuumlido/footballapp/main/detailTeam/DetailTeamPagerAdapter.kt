package com.daarululuumlido.footballapp.main.detailTeam

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.daarululuumlido.footballapp.main.players.PlayersFragment


class DetailTeamPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                DetailTeamOverviewFragment()
            }
            else -> {
                return PlayersFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Overview"
            else -> {
                return "Players"
            }
        }
    }

}