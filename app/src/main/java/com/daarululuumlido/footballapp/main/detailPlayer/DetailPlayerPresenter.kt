package com.daarululuumlido.footballapp.main.detailPlayer

import com.daarululuumlido.footballapp.CoroutineContextProvider
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.model.PlayersResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class DetailPlayerPresenter(private val view: DetailPlayerView,
                            private val apiRepository: ApiRepository,
                            private val gson: Gson,
                            private val context: CoroutineContextProvider = CoroutineContextProvider()) {


    fun getDetailPlayer(idPlayer: String) {
        view.showLoading()
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getPlayer(idPlayer)),
                        PlayersResponse::class.java
                )
            }
            view.initDataPlayer(data.await().players)
            view.hideLoading()
        }
    }

}
