package com.daarululuumlido.footballapp.main.favmatch

import android.util.Log
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.db.RepositoryImpl
import com.daarululuumlido.footballapp.model.Teams
import com.daarululuumlido.footballapp.model.TeamsResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class FavoriteTeamPresenter(private val view: FavoriteTeamView,
                             private val apiRepository: ApiRepository,
                             private val gson: Gson,
                             private val repositoryImpl: RepositoryImpl) {


    fun getDetailTeam() {
        view.showLoading()
        val favList = repositoryImpl.getTeamFromDb()

        var eventList: MutableList<Teams> = mutableListOf()
        for (fav in favList) {
            doAsync {
                val data = gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getTeam(fav.idTeam)),
                        TeamsResponse::class.java
                )
                uiThread {
                    eventList.add(data.teams[0])
                    view.showFavoriteTeamList(eventList)
                }
                view.hideLoading()

            }
        }

        if (favList.isEmpty()) {
            view.hideLoading()
            //view.displayFootballMatch(eventList)
        }
    }
}