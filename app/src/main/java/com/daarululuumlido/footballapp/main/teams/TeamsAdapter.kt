package com.daarululuumlido.footballapp.main.matchlast

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.id.team_badge
import com.daarululuumlido.footballapp.R.id.team_name
import com.daarululuumlido.footballapp.main.detailTeam.DetailTeamActivity
import com.daarululuumlido.footballapp.model.Teams
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick


class TeamsAdapter(private val teams: List<Teams>) :
        RecyclerView.Adapter<TeamsAdapter.TeamsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamsViewHolder {
        return TeamsViewHolder(TeamListUi().createView(AnkoContext.create(parent.context, parent)))

    }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamsViewHolder, position: Int) {
        holder.bindItem(teams[position])
    }


    class TeamsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val teamBadge: ImageView = view.find(team_badge)
        private val teamName: TextView = view.find(team_name)

        fun bindItem(teams: Teams) {
            Picasso.get().load(teams.strTeamBadge).into(teamBadge)
            teamName.text = teams.strTeam
            itemView.onClick {
                it?.context?.startActivity<DetailTeamActivity>(
                        "idTeam" to teams.idTeam
                )
            }
        }
    }
}

class TeamListUi : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {

            linearLayout {
                orientation = LinearLayout.VERTICAL
                padding = dip(18)
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL

                    //Team Badge
                    imageView {
                        id = R.id.team_badge
                    }.lparams {
                        height = dip(50)
                        width = dip(50)
                    }

                    //Team Name
                    textView {
                        id = team_name
                        textSize = 16f
                    }.lparams {
                        margin = dip(15)
                    }
                }.lparams(width = wrapContent, height = wrapContent) {
                    gravity = Gravity.CENTER_HORIZONTAL
                }
            }
        }


    }
}


