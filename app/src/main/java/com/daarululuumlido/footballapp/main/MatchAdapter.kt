package com.daarululuumlido.footballapp.main

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.id.*
import com.daarululuumlido.footballapp.main.detailMatch.DetailScheduleActivity
import com.daarululuumlido.footballapp.model.Match
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat


class MatchAdapter(private val events: List<Match>) :
        RecyclerView.Adapter<MatchAdapter.MacthViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MacthViewHolder {
        return MacthViewHolder(MatchUi().createView(AnkoContext.create(parent.context, parent)))

    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: MacthViewHolder, position: Int) {
        holder.bindItem(events[position])
    }


    class MacthViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        private val Date: TextView = view.find(date_event)
        private val HomeTeam: TextView = view.find(home_team)
        private val AwayTeam: TextView = view.find(away_team)
        private val HomeScore: TextView = view.find(home_score)
        private val AwayScore: TextView = view.find(away_score)


        fun bindItem(events: Match) {

            val date = SimpleDateFormat("E, dd M yyy").format(SimpleDateFormat(
                    "yyyy-MM-dd").parse(events.dateEvent))

            Date.text = date.toString()
            HomeTeam.text = events.strHomeTeam
            AwayTeam.text = events.strAwayTeam
            HomeScore.text = events.intHomeScore
            AwayScore.text = events.intAwayScore

            itemView.onClick {
                it?.context?.startActivity<DetailScheduleActivity>(
                        "idEvent" to events.idEvent,
                        "idHomeTeam" to events.idHomeTeam,
                        "idAwayTeam" to events.idAwayTeam
                )
            }
        }
    }
}

class MatchUi : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            relativeLayout {

                cardView {

                    linearLayout {
                        orientation = LinearLayout.VERTICAL
                        linearLayout {
                            orientation = LinearLayout.HORIZONTAL

                            //DATE EVENT
                            themedTextView(theme = R.style.dateevent) {
                                gravity = Gravity.CENTER_HORIZONTAL
                                id = R.id.date_event
                            }.lparams(width = matchParent, height = wrapContent)
                        }
                        linearLayout {
                            orientation = LinearLayout.HORIZONTAL

                            //HOME TEAM NAME
                            themedTextView(theme = R.style.teamname) {
                                id = R.id.home_team
                                gravity = Gravity.RIGHT or Gravity.CENTER_HORIZONTAL
                            }.lparams(width = dip(0), height = wrapContent) {
                                weight = 2F
                            }
                            //HOME TEAM SCORE
                            themedTextView(theme = R.style.teamscore) {
                                id = R.id.home_score
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = dip(0), height = wrapContent) {
                                weight = 0.35F
                            }
                            textView("vs") {
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = dip(0), height = wrapContent) {
                                weight = 0.25F
                            }
                            //AWAY TEAM SCORE
                            themedTextView(theme = R.style.teamscore) {
                                id = R.id.away_score
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = dip(0), height = wrapContent) {
                                weight = 0.35F
                                margin = dip(2)
                            }
                            //AWAY TEAM NAME
                            themedTextView(theme = R.style.teamname) {
                                id = R.id.away_team
                                gravity = Gravity.LEFT or Gravity.CENTER_HORIZONTAL
                            }.lparams(width = dip(0), height = wrapContent) {
                                weight = 2F
                            }
                        }.lparams(width = matchParent, height = wrapContent) {
                        }
                    }

                }.lparams(width = matchParent, height = dip(80)){
                    elevation =4F
                }
            }
        }


    }
}


