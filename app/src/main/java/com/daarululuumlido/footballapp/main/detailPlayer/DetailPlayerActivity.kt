package com.daarululuumlido.footballapp.main.detailPlayer

import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.invisible
import com.daarululuumlido.footballapp.model.Players
import com.daarululuumlido.footballapp.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_player_detail.*

class DetailPlayerActivity : AppCompatActivity(), DetailPlayerView {

    private lateinit var progressBar: ProgressBar
    private var idPlayer: String = ""
    private lateinit var presenter: DetailPlayerPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_detail)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val intent = intent
        val request = ApiRepository()
        val gson = Gson()

        idPlayer = intent.getStringExtra("idPlayer")
        progressBar = findViewById(R.id.loadingPlayerDetail)
        presenter = DetailPlayerPresenter(this, request, gson)
        presenter.getDetailPlayer(idPlayer)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun initDataPlayer(players: List<Players>) {
        if (!players[0].strFanart1.equals(null)) {
            Glide.with(applicationContext)
                    .load(players[0].strFanart1)
                    .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                    .into(imageBannerPlayer)
        } else {
            Glide.with(applicationContext)
                    .load(players[0].strThumb)
                    .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                    .into(imageBannerPlayer)
        }

        if (!players[0].strCutout.equals(null)) {
            Glide.with(applicationContext)
                    .load(players[0].strCutout)
                    .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                    .into(imgPlayer)
        } else {
            Glide.with(applicationContext)
                    .load(players[0].strThumb)
                    .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                    .into(imgPlayer)
        }


        playerName.text = players[0].strPlayer
        tvPosition.text = players[0].strPosition
        tvDate.text = players[0].dateSigned
        playerOverview.text = players[0].strDescriptionEN

    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

}

