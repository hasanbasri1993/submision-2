package com.daarululuumlido.footballapp.main.detailPlayer

import com.daarululuumlido.footballapp.model.Players

interface DetailPlayerView {
    fun initDataPlayer(data: List<Players>)
    fun hideLoading()
    fun showLoading()
}