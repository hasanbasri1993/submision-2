package com.daarululuumlido.footballapp.main.matchlast

import com.daarululuumlido.footballapp.CoroutineContextProvider
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.model.EventResponse
import com.daarululuumlido.footballapp.model.MatchResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class MatchNextPresenter(private val view: MatchNextView,
                         private val apiRepository: ApiRepository,
                         private val gson: Gson,
                         private val context: CoroutineContextProvider = CoroutineContextProvider()) {


    fun getMacthNext(idLeague: String) {
        view.showLoading()
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getSchedulesNext15(idLeague)),
                        MatchResponse::class.java
                )
            }
            view.showMatchList(data.await().events)
            view.hideLoading()
        }
    }


    fun searchMatch(match: String) {
        view.showLoading()
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.searchSchedule(match)),
                        EventResponse::class.java
                )
            }
            view.showMatchResult(data.await().event)
            view.hideLoading()
        }
    }

}