package com.daarululuumlido.footballapp.main.teams

import android.R
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.*
import com.daarululuumlido.footballapp.R.array.leagueArray
import com.daarululuumlido.footballapp.R.id.*
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.invisible
import com.daarululuumlido.footballapp.main.matchlast.TeamsAdapter
import com.daarululuumlido.footballapp.main.matchlast.TeamsPresenter
import com.daarululuumlido.footballapp.main.matchlast.TeamsView
import com.daarululuumlido.footballapp.model.Teams
import com.daarululuumlido.footballapp.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.abc_search_dropdown_item_icons_2line.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx


class TeamsFragment : Fragment(), AnkoComponent<Context>, TeamsView {

        private lateinit var leagueName: String
    private lateinit var listTeam: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var presenter: TeamsPresenter
    private lateinit var adapter: TeamsAdapter
    private lateinit var spinner: Spinner
    private var teams: MutableList<Teams> = mutableListOf()
    private var teams_result: MutableList<Teams> = mutableListOf()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL
            topPadding = dip(7)
            leftPadding = dip(7)
            rightPadding = dip(7)

            spinner = spinner {
                id = list_time
            }.lparams(width = matchParent, height = wrapContent)

            relativeLayout {
                lparams(width = matchParent, height = wrapContent)

                listTeam = recyclerView {
                    id = com.daarululuumlido.footballapp.R.id.list_team
                    lparams(width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }

                progressBar = progressBar {
                }.lparams {
                    centerHorizontally()
                }


            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(com.daarululuumlido.footballapp.R.menu.menu_search, menu)
        val searchItem = menu.findItem(com.daarululuumlido.footballapp.R.id.menu_search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView
            val editext = searchView.findViewById<EditText>(android.support.v7.appcompat.R.id.search_src_text)
            editext.hint = "Search here..."

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    teams.clear()
                    if (newText.length > 3) {
                        presenter.searchTeam(newText)

                    } else {
                        presenter.getTeamsbyLeague("English%20Premier%20League")
                    }
                    listTeam.adapter.notifyDataSetChanged()
                    return true
                }
            })
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setHasOptionsMenu(true)
        adapter = TeamsAdapter(teams)
        listTeam.adapter = adapter
        val request = ApiRepository()
        val gson = Gson()
        presenter = TeamsPresenter(this, request, gson)
        val spinnerItems = resources.getStringArray(leagueArray)
        val spinnerAdapter = ArrayAdapter(ctx, R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                leagueName = spinner.selectedItem.toString()
                when (leagueName) {
                    "English Premier League" -> presenter.getTeamsbyLeague("English%20Premier%20League")
                    "German Bundesliga" -> presenter.getTeamsbyLeague("German%20Bundesliga")
                    "Italian Serie A" -> presenter.getTeamsbyLeague("Italian%20Serie%20A")
                    "French Ligue 1" -> presenter.getTeamsbyLeague("French%20Ligue%201")
                    "Spanish La Liga" -> presenter.getTeamsbyLeague("Spanish%20La%20Liga")
                    else -> presenter.getTeamsbyLeague("English%20Premier%20League")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showTeamList(data: List<Teams>) {
        teams.clear()
        teams.addAll(data)
        adapter.notifyDataSetChanged()
    }
}