package com.daarululuumlido.footballapp.main.players

import com.daarululuumlido.footballapp.model.Player

interface PlayersView {
    fun showLoading()
    fun hideLoading()
    fun showListPlayer(data: List<Player>)

}