package com.daarululuumlido.footballapp.main.matchlast

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.id.*
import com.daarululuumlido.footballapp.main.detailPlayer.DetailPlayerActivity
import com.daarululuumlido.footballapp.model.Player
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick


class PlayersAdapter(private val player: List<Player>) :
        RecyclerView.Adapter<PlayersAdapter.PlayersViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayersViewHolder {
        return PlayersViewHolder(PlayerListUi().createView(AnkoContext.create(parent.context, parent)))

    }

    override fun getItemCount(): Int = player.size

    override fun onBindViewHolder(holder: PlayersViewHolder, position: Int) {
        holder.bindItem(player[position])
    }


    class PlayersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val playerBadge: ImageView = view.find(imgPlayer)
        private val playerName: TextView = view.find(namePlayer)
        private val playerPosition: TextView = view.find(positionPlayer)

        fun bindItem(player: Player) {
            Picasso.get().load(player.strThumb).into(playerBadge)
            playerName.text = player.strPlayer
            playerPosition.text = player.strPosition
            itemView.onClick {
                it?.context?.startActivity<DetailPlayerActivity>(
                        "idPlayer" to player.idPlayer
                )
            }
        }
    }
}

class PlayerListUi : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {

                linearLayout {
                    orientation = LinearLayout.HORIZONTAL
                    padding = dip(10)
                    weightSum = 3F
                    lparams(height = wrapContent, width = matchParent)

                    imageView {
                        id = R.id.imgPlayer
                    }.lparams(width = wrapContent, height = matchParent) {
                        height = dip(55)
                        width = dip(55)
                        gravity = Gravity.START
                        weight = 0.5F

                    }
                    textView("Nama kuuu") {
                        gravity = Gravity.CENTER_VERTICAL or Gravity.START
                        id = R.id.namePlayer
                        textSize = 16F
                        padding = dip(2)
                    }.lparams(width = 0, height = matchParent) {
                        weight = 1.5F

                    }
                    textView("fghfghgf") {
                        gravity = Gravity.CENTER_VERTICAL or Gravity.END
                        id = R.id.positionPlayer
                        textSize = 14F
                        padding = dip(2)
                    }.lparams(width = 0, height = matchParent) {
                        weight = 1F

                    }
                }



        }


    }
}


