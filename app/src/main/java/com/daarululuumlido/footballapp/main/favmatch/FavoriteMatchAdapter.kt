package com.daarululuumlido.footballapp.main.favmatch


import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.main.detailMatch.DetailScheduleActivity
import com.daarululuumlido.footballapp.model.Match
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat

class FavoriteMacthAdapter(private val favorite: List<Match>)
    : RecyclerView.Adapter<FavoriteTeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteTeamViewHolder {
        return FavoriteTeamViewHolder(MatchUi().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: FavoriteTeamViewHolder, position: Int) {
        holder.bindItem(favorite[position])
    }

    override fun getItemCount(): Int = favorite.size

}

class FavoriteTeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {


    private val Date: TextView = view.find(R.id.date_event)
    private val HomeTeam: TextView = view.find(R.id.home_team)
    private val AwayTeam: TextView = view.find(R.id.away_team)
    private val HomeScore: TextView = view.find(R.id.home_score)
    private val AwayScore: TextView = view.find(R.id.away_score)


    fun bindItem(events: Match) {

        val date = SimpleDateFormat("E, dd-M-yyy").format(SimpleDateFormat(
                "yyyy-MM-dd").parse(events.dateEvent))

        Date.text = date
        HomeTeam.text = events.strHomeTeam
        AwayTeam.text = events.strAwayTeam
        HomeScore.text = events.intHomeScore
        AwayScore.text = events.intAwayScore

        itemView.onClick {
            it?.context?.startActivity<DetailScheduleActivity>(
                    "idEvent" to events.idEvent,
                    "idHomeTeam" to events.idHomeTeam,
                    "idAwayTeam" to events.idAwayTeam
            )
        }
    }
}


class MatchUi : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {

            linearLayout {
                orientation = LinearLayout.VERTICAL
                padding = dip(18)
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL

                    //DATE EVENT
                    themedTextView(theme = R.style.dateevent) {
                        id = R.id.date_event
                    }.lparams(width = wrapContent, height = wrapContent) {
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                }.lparams(width = wrapContent, height = wrapContent) {
                    gravity = Gravity.CENTER_HORIZONTAL
                }
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL
                    padding = dip(5)
                    gravity = Gravity.CENTER_VERTICAL

                    //HOME TEAM NAME
                    themedTextView(theme = R.style.teamname) {
                        id = R.id.home_team
                        gravity = Gravity.RIGHT
                    }.lparams(width = dip(120), height = wrapContent)

                    //HOME TEAM SCORE
                    themedTextView(theme = R.style.teamscore) {
                        id = R.id.home_score
                        gravity = Gravity.CENTER
                    }.lparams(width = dip(20), height = wrapContent)

                    //VS TEXT
                    textView("vs") {
                        gravity = Gravity.CENTER
                        leftPadding = dip(5)
                        rightPadding = dip(5)
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(25), height = wrapContent)

                    //AWAY TEAM SCORE
                    themedTextView(theme = R.style.teamscore) {
                        id = R.id.away_score
                        gravity = Gravity.CENTER
                    }.lparams(width = dip(20), height = wrapContent)

                    //AWAY TEAM NAME
                    themedTextView(theme = R.style.teamname) {
                        id = R.id.away_team
                        gravity = Gravity.LEFT
                    }.lparams(width = dip(120), height = wrapContent)
                }.lparams(width = wrapContent, height = wrapContent)
            }
        }


    }
}