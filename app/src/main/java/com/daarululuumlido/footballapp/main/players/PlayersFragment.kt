package com.daarululuumlido.footballapp.main.players

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.daarululuumlido.footballapp.R.id.list_player
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.invisible
import com.daarululuumlido.footballapp.main.matchlast.PlayersAdapter
import com.daarululuumlido.footballapp.model.Player
import com.daarululuumlido.footballapp.visible
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx


class PlayersFragment : Fragment(), AnkoComponent<Context>, PlayersView {

    private lateinit var progressBar: ProgressBar
    private lateinit var presenter: PlayersPresenter
    private lateinit var adapter: PlayersAdapter
    private lateinit var listPlayer: RecyclerView

    private var players: MutableList<Player> = mutableListOf()
    private var idTeam: String = ""

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = PlayersAdapter(players)
        listPlayer.adapter = adapter
        val request = ApiRepository()
        val gson = Gson()
        idTeam = getActivity()!!.getIntent().getExtras().getString("idTeam")
        presenter = PlayersPresenter(this, request, gson)
        presenter.getPlayers(idTeam)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        relativeLayout {
            lparams(width = matchParent, height = wrapContent)

            listPlayer = recyclerView {
                id = list_player
                lparams(width = matchParent, height = wrapContent)
                layoutManager = LinearLayoutManager(ctx)
            }

            progressBar = progressBar {
            }.lparams {
                centerHorizontally()
            }
        }
    }



    override fun showListPlayer(data: List<Player>) {
        players.clear()
        players.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }


}
