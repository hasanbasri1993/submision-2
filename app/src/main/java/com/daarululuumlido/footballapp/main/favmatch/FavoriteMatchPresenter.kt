package com.daarululuumlido.footballapp.main.favmatch

import android.util.Log
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.db.RepositoryImpl
import com.daarululuumlido.footballapp.model.Match
import com.daarululuumlido.footballapp.model.MatchResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class FavoriteMatchPresenter(private val view: FavoriteMatchView,
                             private val apiRepository: ApiRepository,
                             private val gson: Gson,
                             private val repositoryImpl: RepositoryImpl) {


    fun getDetailSchedule() {
        view.showLoading()
        val favList = repositoryImpl.getMatchFromDb()

        var eventList: MutableList<Match> = mutableListOf()
        for (fav in favList) {
            doAsync {
                val data = gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getDetailSchedule(fav.idEvent)),
                        MatchResponse::class.java
                )
                uiThread {
                    eventList.add(data.events[0])
                    view.showFavoriteMatchList(eventList)
                }
                view.hideLoading()

            }
        }

        if (favList.isEmpty()) {
            view.hideLoading()
            //view.displayFootballMatch(eventList)
        }
    }
}