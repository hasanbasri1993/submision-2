package com.daarululuumlido.footballapp.main.detailTeam

import com.daarululuumlido.footballapp.model.Teams

interface DetailTeamView {
    fun initDataTeam(data: List<Teams>)
    fun hideLoading()
    fun showLoading()
}