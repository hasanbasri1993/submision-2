package com.daarululuumlido.footballapp.main.favmatch


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.db.RepositoryImpl
import com.daarululuumlido.footballapp.invisible
import com.daarululuumlido.footballapp.model.Teams
import com.daarululuumlido.footballapp.visible
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavoriteTeamFragment : Fragment(), AnkoComponent<Context>, FavoriteTeamView {

    private lateinit var adapter: FavoriteTeamAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var presenter: FavoriteTeamPresenter
    private lateinit var progressBar: ProgressBar
    private var favorites: MutableList<Teams> = mutableListOf()
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = FavoriteTeamAdapter(favorites)

        listEvent.adapter = adapter
        val request = ApiRepository()
        val gson = Gson()
        val repositoryImpl = RepositoryImpl(context!!)
        presenter = FavoriteTeamPresenter(this, request, gson, repositoryImpl)
        presenter.getDetailTeam()
        swipeRefresh.onRefresh {
            presenter.getDetailTeam()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)
            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)
                    listEvent = recyclerView {
                        lparams(width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }
                    progressBar = progressBar {
                    }.lparams {
                        centerHorizontally()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }


    override fun showFavoriteTeamList(data: List<Teams>) {
        swipeRefresh.isRefreshing = false
        favorites.clear()
        favorites.addAll(data)
        adapter.notifyDataSetChanged()
    }
}