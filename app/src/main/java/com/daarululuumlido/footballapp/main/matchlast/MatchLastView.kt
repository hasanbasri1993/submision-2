package com.daarululuumlido.footballapp.main.matchlast

import com.daarululuumlido.footballapp.model.Event
import com.daarululuumlido.footballapp.model.Match

interface MatchLastView {
    fun showLoading ()
    fun hideLoading ()
    fun showMatchList (data : List<Match>)
    fun showMatchResult (data : List<Event>)

}