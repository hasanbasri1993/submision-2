package com.daarululuumlido.footballapp.main.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.id.*
import com.daarululuumlido.footballapp.R.layout.activity_home
import com.daarululuumlido.footballapp.main.FavoriteActivity
import com.daarululuumlido.footballapp.main.MatchActivity
import com.daarululuumlido.footballapp.main.teams.TeamsFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_home)

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                teams -> {
                    loadTeamsFragment(savedInstanceState)
                }

                match -> {
                    loadMatchFragment(savedInstanceState)
                }

                favorites -> {
                    loadFavoritesFragment(savedInstanceState)
                }
            }
            true
        }
        bottom_navigation.selectedItemId = teams
    }

    private fun loadTeamsFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, TeamsFragment(), TeamsFragment::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, MatchActivity(), MatchActivity::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadFavoritesFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, FavoriteActivity(), FavoriteActivity::class.java.simpleName)
                    .commit()
        }
    }
}
