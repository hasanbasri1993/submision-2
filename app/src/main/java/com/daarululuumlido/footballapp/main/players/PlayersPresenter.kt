package com.daarululuumlido.footballapp.main.players

import com.daarululuumlido.footballapp.CoroutineContextProvider
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.model.PlayerResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PlayersPresenter(private val view: PlayersView,
                       private val apiRepository: ApiRepository,
                       private val gson: Gson,
                       private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getPlayers(idTeam: String) {
        view.showLoading()
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getAllPlayerbyTeam(idTeam)),
                        PlayerResponse::class.java
                )
            }
            view.showListPlayer(data.await().player)
            view.hideLoading()
        }
    }

}
