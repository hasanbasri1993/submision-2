package com.daarululuumlido.footballapp.main.matchlast

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.*
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.color.colorAccent
import com.daarululuumlido.footballapp.R.id.list_match
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.invisible
import com.daarululuumlido.footballapp.main.MatchAdapter
import com.daarululuumlido.footballapp.main.MatchResultAdapter
import com.daarululuumlido.footballapp.model.Event
import com.daarululuumlido.footballapp.model.Match
import com.daarululuumlido.footballapp.visible
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class MatchLastFragment : Fragment(), AnkoComponent<Context>, MatchLastView {

    private lateinit var listTeam: RecyclerView
    private lateinit var leagueName: String
    private lateinit var progressBar: ProgressBar
    private lateinit var presenter: MatchLastPresenter
    private lateinit var adapter: MatchAdapter
    private lateinit var adapterResult: MatchResultAdapter
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var spinner: Spinner

    private var events: MutableList<Match> = mutableListOf()
    private var event: MutableList<Event> = mutableListOf()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(com.daarululuumlido.footballapp.R.menu.menu_search, menu)
        val searchItem = menu.findItem(com.daarululuumlido.footballapp.R.id.menu_search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView
            val editext = searchView.findViewById<EditText>(android.support.v7.appcompat.R.id.search_src_text)
            editext.hint = "Search here..."

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    events.clear()
                    event.clear()
                    if (newText.length > 3) {
                        adapterResult = MatchResultAdapter(event)
                        listTeam.adapter = adapterResult
                        presenter.searchMatch(newText)

                    } else {
                        adapter = MatchAdapter(events)
                        listTeam.adapter = adapter
                        leagueName = spinner.selectedItem.toString()
                        when (leagueName) {
                            "English Premier League" -> presenter.getMacthLast("4328")
                            "German Bundesliga" -> presenter.getMacthLast("4331")
                            "Italian Serie A" -> presenter.getMacthLast("4332")
                            "French Ligue 1" -> presenter.getMacthLast("4334")
                            "Spanish La Liga" -> presenter.getMacthLast("4335")
                            else -> presenter.getMacthLast("4328")
                        }
                    }
                    listTeam.adapter.notifyDataSetChanged()
                    return true
                }
            })
        }
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL
            topPadding = dip(7)
            leftPadding = dip(7)
            rightPadding = dip(7)

            spinner = spinner {
                id = R.id.list_league
            }.lparams(width = matchParent, height = wrapContent)

            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)


                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    listTeam = recyclerView {
                        id = list_match
                        lparams(width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams {
                        centerHorizontally()
                    }


                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true);
        adapter = MatchAdapter(events)
        listTeam.adapter = adapter
        val request = ApiRepository()
        val gson = Gson()
        presenter = MatchLastPresenter(this, request, gson)
        val spinnerItems = resources.getStringArray(R.array.leagueArray)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                leagueName = spinner.selectedItem.toString()
                when (leagueName) {
                    "English Premier League" -> presenter.getMacthLast("4328")
                    "German Bundesliga" -> presenter.getMacthLast("4331")
                    "Italian Serie A" -> presenter.getMacthLast("4332")
                    "French Ligue 1" -> presenter.getMacthLast("4334")
                    "Spanish La Liga" -> presenter.getMacthLast("4335")
                    else -> presenter.getMacthLast("4328")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        swipeRefresh.onRefresh {
            leagueName = spinner.selectedItem.toString()
            when (leagueName) {
                "English Premier League" -> presenter.getMacthLast("4328")
                "German Bundesliga" -> presenter.getMacthLast("4331")
                "Italian Serie A" -> presenter.getMacthLast("4332")
                "French Ligue 1" -> presenter.getMacthLast("4334")
                "Spanish La Liga" -> presenter.getMacthLast("4335")
                else -> presenter.getMacthLast("4328")
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showMatchList(data: List<Match>) {
        swipeRefresh.isRefreshing = false
        events.clear()
        events.addAll(data)
        adapter.notifyDataSetChanged()
    }


    override fun showMatchResult(data: List<Event>) {
        event.clear()
        event.addAll(data)
        adapterResult.notifyDataSetChanged()
    }
}
