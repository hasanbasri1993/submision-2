package com.daarululuumlido.footballapp.main.favmatch


import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.main.detailMatch.DetailScheduleActivity
import com.daarululuumlido.footballapp.main.detailTeam.DetailTeamActivity
import com.daarululuumlido.footballapp.model.Teams
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class FavoriteTeamAdapter(private val favorite: List<Teams>)
    : RecyclerView.Adapter<FavoriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder(TeamUi().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bindItem(favorite[position])
    }

    override fun getItemCount(): Int = favorite.size

}

class FavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val teamNameFav: TextView = view.find(R.id.teamNameFav)
    private val teamBadgeFav: ImageView = view.find(R.id.teamBadgeFav)


    fun bindItem(team: Teams) {

        teamNameFav.text = team.strTeam
        Picasso.get().load(team.strTeamBadge).into(teamBadgeFav)
        itemView.onClick {
            it?.context?.startActivity<DetailTeamActivity>(
                    "idTeam" to team.idTeam
            )
        }
    }
}


class TeamUi : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                orientation = LinearLayout.HORIZONTAL
                padding = dip(5)
                gravity = Gravity.CENTER_VERTICAL

                //Team Badge
                imageView {
                    id = R.id.teamBadgeFav
                }.lparams {
                    height = dip(50)
                    width = dip(50)
                }

                //HOME TEAM NAME
                themedTextView(theme = R.style.teamname) {
                    id = R.id.teamNameFav
                    gravity = Gravity.RIGHT
                }.lparams(width = dip(120), height = wrapContent)
            }
        }
    }

}
