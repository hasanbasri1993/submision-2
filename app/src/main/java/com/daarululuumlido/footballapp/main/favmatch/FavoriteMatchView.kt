package com.daarululuumlido.footballapp.main.favmatch

import com.daarululuumlido.footballapp.model.Match

interface FavoriteMatchView {
    fun showLoading ()
    fun hideLoading ()
    fun showFavoriteMatchList (data : List<Match>)
}