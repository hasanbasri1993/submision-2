package com.daarululuumlido.footballapp.main.favmatch

import com.daarululuumlido.footballapp.model.Teams

interface FavoriteTeamView {
    fun showLoading ()
    fun hideLoading ()
    fun showFavoriteTeamList (data : List<Teams>)
}