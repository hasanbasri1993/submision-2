package com.daarululuumlido.footballapp.main.detailMatch

import com.daarululuumlido.footballapp.model.Match
import com.daarululuumlido.footballapp.model.Teams

interface DetailScheduleView {
    fun displayTeamBadgeHome(team: Teams)
    fun displayTeamBadgeAway(team: Teams)
    fun initData (data : List<Match>)
}