package com.daarululuumlido.footballapp.main.matchlast

import com.daarululuumlido.footballapp.model.Teams

interface TeamsView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Teams>)
}