package com.daarululuumlido.footballapp.main.detailMatch

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.drawable.ic_add_to_favorites
import com.daarululuumlido.footballapp.R.drawable.ic_added_to_favorites
import com.daarululuumlido.footballapp.R.menu.menu_favorite
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.db.FavoriteMatch
import com.daarululuumlido.footballapp.db.database
import com.daarululuumlido.footballapp.model.Match
import com.daarululuumlido.footballapp.model.Teams
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_macth_detail.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat

class DetailScheduleActivity : AppCompatActivity(), DetailScheduleView {

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var idEvent: String = ""
    private var idHome: String = ""
    private var idAway: String = ""
    private var events: MutableList<Match> = mutableListOf()
    lateinit var presenter: DetailSchedulePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_macth_detail)

        //supportActionBar!!.title = "Go Back"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val intent = intent
        val request = ApiRepository()
        val gson = Gson()

        idEvent = intent.getStringExtra("idEvent")
        idAway = intent.getStringExtra("idHomeTeam")
        idHome = intent.getStringExtra("idAwayTeam")
        favoriteState()
        presenter = DetailSchedulePresenter(this, request, gson)
        presenter.getDetailSchedule(idEvent)
        presenter.getTeamBadgeAway(idAway)
        presenter.getTeamBadgeHome(idHome)
    }

    override fun initData(event: List<Match>) {

        events.clear()
        events.addAll(event)

        val date = SimpleDateFormat("E, dd-M-yyy").format(SimpleDateFormat(
                "yyyy-MM-dd").parse(event[0].dateEvent))

        dateScheduleTv.text = date
        homeNameTv.text = event[0].strHomeTeam
        homeScoreTv.text = event[0].intHomeScore
        awayNameTv.text = event[0].strAwayTeam
        awayScoreTv.text = event[0].intAwayScore

        homeScorerTv.text = event[0].strHomeGoalDetails
        awayScorerTv.text = event[0].strAwayGoalDetails

        gkHomeTv.text = event[0].strHomeLineupGoalkeeper
        gkAwayTv.text = event[0].strAwayLineupGoalkeeper

        defHomeTv.text = event[0].strHomeLineupDefense
        defAwayTv.text = event[0].strAwayLineupDefense

        midHomeTv.text = event[0].strHomeLineupMidfield
        midAwayTv.text = event[0].strAwayLineupMidfield

        subHomeTv.text = event[0].strHomeLineupSubstitutes
        subAwayTv.text = event[0].strAwayLineupSubstitutes

    }

    override fun displayTeamBadgeAway(team: Teams) {
        val homeBadge = find(R.id.awayImg) as ImageView
        Glide.with(this)
                .load(team.strTeamBadge)
                .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                .into(homeBadge)
    }

    override fun displayTeamBadgeHome(team: Teams) {
        val awayBadge = find(R.id.homeImg) as ImageView
        Glide.with(this)
                .load(team.strTeamBadge)
                .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                .into(awayBadge)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate((menu_favorite), menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite(idEvent) else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(FavoriteMatch.TABLE_FAVMATCH,
                        FavoriteMatch.EVENT_ID to idEvent,
                        FavoriteMatch.AWAY_TEAM_ID to idAway,
                        FavoriteMatch.HOME_TEAM_ID to idHome)
            }
            toast("Added to favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun removeFromFavorite(eventId: String) {
        try {
            database.use {
                delete(FavoriteMatch.TABLE_FAVMATCH, "(EVENT_ID = {id})",
                        "id" to eventId)
            }
            toast("Removed to favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }

    }

    private fun setFavorite() {

        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    private fun favoriteState() {
        database.use {
            val result = select(FavoriteMatch.TABLE_FAVMATCH)
                    .whereArgs("(EVENT_ID = {id})",
                            "id" to idEvent)
            val favorite = result.parseList(classParser<FavoriteMatch>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

}

