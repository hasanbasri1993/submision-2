package com.daarululuumlido.footballapp.main.detailTeam

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.drawable.ic_add_to_favorites
import com.daarululuumlido.footballapp.R.drawable.ic_added_to_favorites
import com.daarululuumlido.footballapp.R.menu.menu_favorite
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.db.FavoriteTeam
import com.daarululuumlido.footballapp.db.database
import com.daarululuumlido.footballapp.invisible
import com.daarululuumlido.footballapp.model.Teams
import com.daarululuumlido.footballapp.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_team_detail.*
import kotlinx.android.synthetic.main.fragment_team_overview.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class DetailTeamActivity : AppCompatActivity(), DetailTeamView {

    private lateinit var progressBar: ProgressBar
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var idTeam: String = ""
    private lateinit var presenter: DetailTeamPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_detail)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val fragmentAdapter = DetailTeamPagerAdapter(supportFragmentManager)
        val intent = intent
        val request = ApiRepository()
        val gson = Gson()
        viewpager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewpager)
        progressBar = findViewById(R.id.loadingTeamDetail)
        idTeam = intent.getStringExtra("idTeam")
        presenter = DetailTeamPresenter(this, request, gson)
        presenter.getDetailTeam(idTeam)
        favoriteState()
    }


    override fun initDataTeam(team: List<Teams>) {

        if (!team[0].strTeamFanart1.equals(null)) {
            Glide.with(applicationContext)
                    .load(team[0].strTeamFanart1)
                    .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                    .apply(RequestOptions().override(220, 160))
                    .into(imageTeam)
        } else {
            Glide.with(applicationContext)
                    .load(team[0].strTeamBadge)
                    .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                    .apply(RequestOptions().override(120, 140))
                    .into(imageTeam)
        }
        Glide.with(applicationContext)
                .load(team[0].strTeamBadge)
                .apply(RequestOptions().placeholder(R.drawable.abc_ic_star_black_16dp))
                .apply(RequestOptions().override(120, 140))
                .into(imgBadge)
        teamName.text = team[0].strTeam
        tvStadium.text = team[0].strStadium
        tvManager.text = team[0].strManager
        teamOverview.text = team[0].strDescriptionEN
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate((menu_favorite), menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite(idTeam) else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(FavoriteTeam.TABLE_FAVTEAM,
                        FavoriteTeam.TEAM_ID to idTeam)
            }
            toast("Added to favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun removeFromFavorite(eventId: String) {
        try {
            database.use {
                delete(FavoriteTeam.TABLE_FAVTEAM, "(TEAM_ID = {id})",
                        "id" to eventId)
            }
            toast("Removed to favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }

    }

    private fun setFavorite() {

        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    private fun favoriteState() {
        database.use {
            val result = select(FavoriteTeam.TABLE_FAVTEAM)
                    .whereArgs("(TEAM_ID = {id})",
                            "id" to idTeam)
            val favorite = result.parseList(classParser<FavoriteTeam>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

}

