package com.daarululuumlido.footballapp.main.detailMatch

import com.daarululuumlido.footballapp.CoroutineContextProvider
import com.daarululuumlido.footballapp.api.ApiRepository
import com.daarululuumlido.footballapp.api.TheSportDBAPi
import com.daarululuumlido.footballapp.model.MatchResponse
import com.daarululuumlido.footballapp.model.TeamsResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class DetailSchedulePresenter(private val view: DetailScheduleView,
                              private val apiRepository: ApiRepository,
                              private val gson: Gson,
                              private val context: CoroutineContextProvider = CoroutineContextProvider()) {


    fun getDetailSchedule(id_League: String) {
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getDetailSchedule(id_League)),
                        MatchResponse::class.java
                )
            }

            view.initData(data.await().events)


        }
    }


    fun getTeamBadgeAway(id_League: String) {
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getTeam(id_League)),
                        TeamsResponse::class.java
                )
            }

            view.displayTeamBadgeAway(data.await().teams[0])
        }
    }

    fun getTeamBadgeHome(id_League: String) {
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportDBAPi.getTeam(id_League)),
                        TeamsResponse::class.java
                )
            }

            view.displayTeamBadgeHome(data.await().teams[0])
        }
    }
}
