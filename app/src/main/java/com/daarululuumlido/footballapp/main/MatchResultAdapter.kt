package com.daarululuumlido.footballapp.main

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.daarululuumlido.footballapp.R
import com.daarululuumlido.footballapp.R.id.*
import com.daarululuumlido.footballapp.main.detailMatch.DetailScheduleActivity
import com.daarululuumlido.footballapp.model.Match
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat
import android.support.v4.content.ContextCompat.startActivity
import android.provider.CalendarContract.Events
import android.provider.CalendarContract
import android.content.Intent
import com.daarululuumlido.footballapp.model.Event
import java.util.*


class MatchResultAdapter(private val event: List<Event>) :
        RecyclerView.Adapter<MatchResultAdapter.MacthViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MacthViewHolder {
        return MacthViewHolder(MatchResultUi().createView(AnkoContext.create(parent.context, parent)))

    }

    override fun getItemCount(): Int = event.size

    override fun onBindViewHolder(holder: MacthViewHolder, position: Int) {
        holder.bindItem(event[position])
    }


    class MacthViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        private val Date: TextView = view.find(date_event)
        private val HomeTeam: TextView = view.find(home_team)
        private val AwayTeam: TextView = view.find(away_team)
        private val HomeScore: TextView = view.find(home_score)
        private val AwayScore: TextView = view.find(away_score)


        fun bindItem(event: Event) {

            val date = SimpleDateFormat("E, dd M yyy").format(SimpleDateFormat(
                    "yyyy-MM-dd").parse(event.dateEvent))

            Date.text = date.toString()
            HomeTeam.text = event.strHomeTeam
            AwayTeam.text = event.strAwayTeam
            HomeScore.text = event.intHomeScore
            AwayScore.text = event.intAwayScore

            itemView.onClick {
                it?.context?.startActivity<DetailScheduleActivity>(
                        "idEvent" to event.idEvent,
                        "idHomeTeam" to event.idHomeTeam,
                        "idAwayTeam" to event.idAwayTeam
                )
            }
        }
    }
}

class MatchResultUi : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {

            linearLayout {
                orientation = LinearLayout.VERTICAL
                padding = dip(18)
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL

                    //DATE EVENT
                    themedTextView(theme = R.style.dateevent) {
                        id = R.id.date_event
                    }.lparams(width = wrapContent, height = wrapContent) {
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                }.lparams(width = wrapContent, height = wrapContent) {
                    gravity = Gravity.CENTER_HORIZONTAL
                }
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL
                    padding = dip(5)
                    gravity = Gravity.CENTER_VERTICAL

                    //HOME TEAM NAME
                    themedTextView(theme = R.style.teamname) {
                        id = R.id.home_team
                        gravity = Gravity.RIGHT
                    }.lparams(width = dip(120), height = wrapContent)

                    //HOME TEAM SCORE
                    themedTextView(theme = R.style.teamscore) {
                        id = R.id.home_score
                        gravity = Gravity.CENTER
                    }.lparams(width = dip(20), height = wrapContent)

                    //VS TEXT
                    textView("vs") {
                        gravity = Gravity.CENTER
                        leftPadding = dip(5)
                        rightPadding = dip(5)
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(25), height = wrapContent)

                    //AWAY TEAM SCORE
                    themedTextView(theme = R.style.teamscore) {
                        id = R.id.away_score
                        gravity = Gravity.CENTER
                    }.lparams(width = dip(20), height = wrapContent)

                    //AWAY TEAM NAME
                    themedTextView(theme = R.style.teamname) {
                        id = R.id.away_team
                        gravity = Gravity.LEFT
                    }.lparams(width = dip(120), height = wrapContent)
                }.lparams(width = wrapContent, height = wrapContent)
            }
        }


    }
}


