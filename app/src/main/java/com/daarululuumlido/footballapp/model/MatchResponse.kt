package com.daarululuumlido.footballapp.model

import com.google.gson.annotations.SerializedName

data class MatchResponse(

        @field:SerializedName("events")
        val events: List<Match>
)