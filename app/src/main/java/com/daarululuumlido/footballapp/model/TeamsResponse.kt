package com.daarululuumlido.footballapp.model

import com.google.gson.annotations.SerializedName

data class TeamsResponse(

        @field:SerializedName("teams")
        val teams: List<Teams>
)