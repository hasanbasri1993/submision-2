package com.daarululuumlido.footballapp.model

import com.google.gson.annotations.SerializedName

data class EventResponse(

	@field:SerializedName("event")
	val event: List<Event>
)