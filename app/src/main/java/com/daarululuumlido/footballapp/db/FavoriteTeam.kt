package com.daarululuumlido.footballapp.db


data class FavoriteTeam(
        val id: Long?,
        val idTeam: String

) {
    companion object {
        const val TABLE_FAVTEAM: String = "TABLE_FAVTEAM"
        const val ID: String = "ID_"
        const val TEAM_ID: String = "TEAM_ID"
    }

}
