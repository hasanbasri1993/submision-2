package com.daarululuumlido.footballapp.db

interface Repository {

    fun getMatchFromDb(): List<FavoriteMatch>
    fun getTeamFromDb(): List<FavoriteTeam>

}
