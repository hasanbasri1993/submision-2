package com.daarululuumlido.footballapp.db

import android.content.Context
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select


class RepositoryImpl(private val context: Context) : Repository {

    override fun getMatchFromDb(): List<FavoriteMatch> {
        lateinit var favoriteList: List<FavoriteMatch>
        context.database.use {
            val result = select(FavoriteMatch.TABLE_FAVMATCH)
            val favorite = result.parseList(classParser<FavoriteMatch>())
            favoriteList = favorite
        }
        return favoriteList
    }

    override fun getTeamFromDb(): List<FavoriteTeam> {
        lateinit var favoriteList: List<FavoriteTeam>
        context.database.use {
            val result = select(FavoriteTeam.TABLE_FAVTEAM)
            val favorite = result.parseList(classParser<FavoriteTeam>())
            favoriteList = favorite
        }
        return favoriteList
    }
}